# Frontend



### Install jest and puppeteer

```bash
# By npm

# Step 1
npm install --save-dev jest-puppeteer@4.4.0 

# Step 2
npm install --save-dev puppeteer jest

# OR By yarn

yarn add jest-puppeteer@4.4.0 puppeteer jest --dev


```

* ปัจจุบัน puppeteer ที่ Published แล้ว อยู่ที่ version @7.0.4

แต่ตอนติดตั้ง กลับได้ versio @2.1.1 เพราะ jest-puppeteer version @4.4.0 ร้องขอ puppeteer version ระหว่าง >= 1.5.0 และ <3 เท่านั้น จึงเกิดเหตุการ dependency conflict


### Run Test by jest-puppeteer

```bash

npm run test

```
